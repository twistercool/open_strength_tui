# open\_strength\_tui

In order to build the project, navigate to `./build` and run the command `make`.
You can then run the binary with `./open_strength_tui`.

You can get the debug binary with `make debug` and run it with `./debug`.
