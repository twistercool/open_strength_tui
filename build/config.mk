VERSION = 0.1
INCS = -I ../include
LIBS = -lpierre -lncurses -lopen_strength -lform
CFLAGS += -std=c17 ${INCS} -DVERSION=\"${VERSION}\" -Ofast -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable
LDFLAGS += ${LIBS}
DEBUG_CFLAGS = ${CFLAGS} -O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter
CC = gcc
