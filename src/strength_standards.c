#include "header.h"

void print_standards_table(gender, double, int, bool);

void print_menu_2_strength_standards() {
   int selected = 0, ch = 0, rows, cols;
   getmaxyx(stdscr, rows, cols);

   FIELD* fields[8];

   fields[0] = new_field(1, 17, 2, 2, 0, 0);  // label bw
   fields[1] = new_field(1, 5, 4, 2, 0, 0);   // input bw

   fields[2] = new_field(1, 21, 6, 2, 0, 0);  // label age
   fields[3] = new_field(1, 5, 8, 2, 0, 0);   // input age

   fields[4] = new_field(1, 16, 10, 2, 0, 0);  // label sex
   fields[5] = new_field(1, 5, 12, 2, 0, 0);   // input sex

   fields[6] = new_field(1, 8, 14, 2, 0, 0);  // label previous

   fields[7] = NULL;

   // specifying the field options
   field_opts_off(fields[0], O_ACTIVE);
   set_field_buffer(fields[0], 0, "Input bodyweight:");

   field_opts_off(fields[2], O_ACTIVE);
   set_field_buffer(fields[2], 0, "Input age (optional):");

   field_opts_off(fields[4], O_ACTIVE);
   set_field_buffer(fields[4], 0, "Input sex (M/f):");

   field_opts_off(fields[6], O_EDIT);
   set_field_buffer(fields[6], 0, "Previous");

   FORM* form = new_form(fields);
   // turns off going to previous lines
   form_opts_off(form, O_BS_OVERLOAD);
   form_opts_off(form, O_AUTOSKIP);
   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < 3) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_LEFT:
            form_driver(form, REQ_PREV_CHAR);
            break;
         case KEY_RIGHT:
            form_driver(form, REQ_NEXT_CHAR);
            break;
         case ENTER:
         case SPACE:

            if (selected == 3) {
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < 7; i++) free_field(fields[i]);
               clear();
               curs_set(false);
               return;
            } else {
               // validate inputs and put the table

               // reloads the form values
               form_driver(form, REQ_END_LINE);

               char* field_buf_0 = field_buffer(fields[1], 0);
               char* field_buf_1 = field_buffer(fields[3], 0);
               char* field_buf_2 = field_buffer(fields[5], 0);

               double input_bw = atof(field_buf_0);
               int input_age = atoi(field_buf_1);
               gender input_gend =
                   (tolower(field_buf_0[2]) == 'f') ? female : male;

               clear();
               unpost_form(form);

               // print small version if there's not enough rows or cols
               print_standards_table(
                   input_gend, input_bw, input_age,
                   (rows < NB_LIFTS * 2 || cols < NB_STRENGTH_CATEGORIES * 14));

               post_form(form);
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:
            form_driver(form, ch);
            break;
      }

      box(stdscr, 2, 0);

      getmaxyx(stdscr, rows, cols);

      for (int i = 0; i < 7; i++) set_field_back(fields[i], A_NORMAL);

      (selected == 3) ? curs_set(false) : curs_set(true);

      set_field_back(fields[2 * selected], A_STANDOUT);

      refresh();
   } while ((ch = getch()));
}

double score_from_index[] = {30, 45, 60, 75, 87.5, 100, 112.5, 125};
// all names are 8 or less char
const char* const small_lift_names[NB_LIFTS] = {
    "Squat", "F.Squat", "Conv.Dl", "Sumo Dl", "Pow.Cl.", "Bench",  "Inc.BP",
    "Dip",   "Ohp",     "Psh.Prs", "Sna.Prs", "Chinup",  "Pullup", "Pen.Row"};
const char* const small_strength_categories[NB_STRENGTH_CATEGORIES] = {
    "Sub",  "Untrnd.", "Novice", "Inter.", "Prof.",
    "Adv.", "Except.", "Elite",  "World"};

extern void setColour(double score);
extern void printColour(double score, const char* input);

void print_standards_table(gender gend, double bw, int age,
                           bool small_version) {
   curs_set(false);

   int ch = 0;
   // start at y==0 and x==0
   unsigned cur_row = 0, cur_col = 0;
   do {
      switch (ch) {
         case KEY_BACKSPACE:
         case (int)'q':
            clear();
            curs_set(false);
            return;
         default:
            break;
      }

      // refresh screen

      // for each lift
      for (int i = 0; i < 14 + 1; i++) {
         // for each category
         for (int j = 0; j < NB_STRENGTH_CATEGORIES; j++) {
            (small_version) ? move(i, 7 * j) : move(2 * i - cur_row, 14 * j);

            if (j == 0 && i == 0) {
               printw("Lifts");
               continue;
            }

            if (i == 0) {
               // print the strength category, except subpar
               printColour(score_from_index[j],
                           (small_version) ? small_strength_categories[j]
                                           : strength_categories[j]);
               continue;
            }

            if (j == 0) {
               // print the lift's name
               printw("%s", (small_version) ? small_lift_names[i - 1]
                                            : lift_names[i - 1]);
               continue;
            }

            printw("%.0f",
                   getLiftFromScore(score_from_index[j - 1], (lift)i - 1,
                                    (double)bw, gend, age));
         }
      }
      refresh();
   } while ((ch = getch()));
}
