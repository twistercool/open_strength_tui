#include "header.h"

void print_menu_3_score_get_best_sets(lift[], int);
void print_menu_4_score_get_stats(lift[], double[], size_t);
void print_menu_5_score_show_results(lift[], double[], size_t, gender, double,
                                     unsigned short);

void setColour(double score);

void print_menu_2_score_select_lifts() {
   int ch = 0, selected = 0;
   bool lifts_selected[NB_LIFTS];
   for (int i = 0; i < NB_LIFTS; i++) lifts_selected[i] = false;

   // field for each lifts and one for Previous
   FIELD* fields[NB_LIFTS + 2];
   for (int i = 0; i < NB_LIFTS + 1; i++) {
      fields[i] = new_field(1, 14, 10 + i, 10, 0, 0);
   }
   fields[NB_LIFTS + 1] = NULL;

   // field options
   for (int i = 0; i < NB_LIFTS + 1; i++) {
      field_opts_off(fields[i], O_EDIT);
      (i == NB_LIFTS) ? set_field_buffer(fields[i], 0, "Previous")
                      : set_field_buffer(fields[i], 0, lift_names[i]);
   }

   FORM* form = new_form(fields);
   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < NB_LIFTS) {
               form_driver(form, REQ_NEXT_FIELD);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               selected--;
            }
            break;
         case ENTER:
            if (selected == NB_LIFTS) {
               clear();
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < NB_LIFTS + 1; i++) free_field(fields[i]);
               return;
            } else {
               int nb_selected = 0;
               for (int i = 0; i < NB_LIFTS; i++)
                  nb_selected += lifts_selected[i];

               if (nb_selected > 0) {
                  unpost_form(form);
                  lift lifts_selected_indices[nb_selected];
                  int index_to_add = 0;
                  for (int i = 0; i < NB_LIFTS; i++) {
                     if (lifts_selected[i] == true) {
                        lifts_selected_indices[index_to_add] = (lift)i;
                        index_to_add++;
                     }
                  }

                  print_menu_3_score_get_best_sets(lifts_selected_indices,
                                                   nb_selected);
                  post_form(form);
               }
            }
            break;

         case 'q':
            clear();
            unpost_form(form);
            free_form(form);
            for (int i = 0; i < NB_LIFTS + 1; i++) free_field(fields[i]);
            return;
         case SPACE:
            if (selected == NB_LIFTS) {  // previous
               clear();
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < NB_LIFTS + 1; i++) free_field(fields[i]);
               return;
            } else {
               lifts_selected[selected] = !lifts_selected[selected];
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:
            form_driver(form, ch);
            break;
      }

      box(stdscr, 2, 0);

      for (int i = 0; i < NB_LIFTS + 1; i++)
         set_field_back(fields[i], A_NORMAL);

      for (int i = 0; i < NB_LIFTS; i++) {
         move(10 + i, 8);
         if (lifts_selected[i])
            printw("*");
         else
            printw(" ");
      }

      set_field_back(fields[selected], A_STANDOUT);

      refresh();
   } while ((ch = getch()));
}

void print_menu_3_score_get_best_sets(lift lifts_selected[], int nb_selected) {
   curs_set(true);

   int ch = 0, selected = 0;

   // includes NULL, previous, etc...
   const int NB_FIELDS = nb_selected * 3 + 4;

   // init field array
   // 1 label, 2 inputs (weight and reps) for each lift, and previous and
   // NULL as well as the label for the weight and for the reps
   FIELD* fields[NB_FIELDS];

   // init the fields for all selected lifts
   for (int i = 0; i < nb_selected; i++) {
      fields[i * 3] = new_field(1, 13, 2 + i * 2, 10, 0, 0);
      fields[i * 3 + 1] = new_field(1, 5, 2 + i * 2, 24, 0, 0);
      fields[i * 3 + 2] = new_field(1, 5, 2 + i * 2, 34, 0, 0);

      field_opts_off(fields[i * 3], O_ACTIVE);

      // sets the lift name
      set_field_buffer(fields[i * 3], 0, lift_names[lifts_selected[i]]);

      field_opts_off(fields[i * 3 + 1], O_AUTOSKIP);
      field_opts_off(fields[i * 3 + 2], O_AUTOSKIP);
   }

   fields[NB_FIELDS - 4] = new_field(1, 8, 4 + 2 * nb_selected, 10, 0, 0);
   set_field_buffer(fields[NB_FIELDS - 4], 0, "Previous");

   fields[NB_FIELDS - 3] = new_field(1, 8, 1, 24, 0, 0);
   set_field_buffer(fields[NB_FIELDS - 3], 0, "Weight:");

   fields[NB_FIELDS - 2] = new_field(1, 19, 1, 34, 0, 0);
   set_field_buffer(fields[NB_FIELDS - 2], 0, "Reps (Optional):");

   fields[NB_FIELDS - 1] = NULL;

   FORM* form = new_form(fields);
   form_opts_off(form, O_BS_OVERLOAD);

   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < nb_selected * 2) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            if (selected < nb_selected * 2) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_LEFT:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_RIGHT:
            if (selected < nb_selected * 2) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case 'q':
            unpost_form(form);
            free_form(form);
            for (int i = 0; i < nb_selected * 3 + 3; i++) free_field(fields[i]);
            curs_set(false);
            clear();
            return;

         case ENTER:
            if (selected == nb_selected * 2) {
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < nb_selected * 3 + 3; i++)
                  free_field(fields[i]);
               curs_set(false);
               clear();
               return;
            } else {
               form_driver(form, REQ_END_LINE);
               // calculate estimated 1RMs for all lifts and put them into an
               // array
               char* input_weights[nb_selected];
               char* input_reps[nb_selected];

               for (int i = 0; i < nb_selected; i++)
                  input_weights[i] = field_buffer(fields[3 * i + 1], 0);
               for (int i = 0; i < nb_selected; i++)
                  input_reps[i] = field_buffer(fields[3 * i + 2], 0);

               double estimated_1RMs[nb_selected];
               for (int i = 0; i < nb_selected; i++) {
                  double input_weight = atof(input_weights[i]);
                  unsigned short input_rep = (!strcmp(input_reps[i], "     "))
                                                 ? 1
                                                 : atoi(input_reps[i]);
                  estimated_1RMs[i] = get1RM(input_weight, input_rep);
               }

               clear();
               unpost_form(form);
               print_menu_4_score_get_stats(lifts_selected, estimated_1RMs,
                                            (size_t)nb_selected);
               post_form(form);
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:
            if ((ch >= '0' && ch <= '9') || ch == '.') form_driver(form, ch);
            break;
      }

      box(stdscr, 2, 0);

      (selected == nb_selected * 2) ? curs_set(false) : curs_set(true);

      for (int i = 0; i < NB_FIELDS - 3; i++)
         set_field_back(fields[i], A_NORMAL);

      int highlighted =
          (selected == nb_selected * 2) ? NB_FIELDS - 4 : (selected / 2) * 3;
      set_field_back(fields[highlighted], A_STANDOUT);

      refresh();
   } while ((ch = getch()));
}

void print_menu_4_score_get_stats(lift input_lifts[], double estimated_1RMs[],
                                  size_t nb_lifts) {
   int ch = 0, selected = 0;

   FIELD* fields[8];

   fields[0] = new_field(1, 17, 2, 2, 0, 0);  // label bw
   fields[1] = new_field(1, 5, 4, 2, 0, 0);   // input bw

   fields[2] = new_field(1, 21, 6, 2, 0, 0);  // label age
   fields[3] = new_field(1, 5, 8, 2, 0, 0);   // input age

   fields[4] = new_field(1, 16, 10, 2, 0, 0);  // label sex
   fields[5] = new_field(1, 5, 12, 2, 0, 0);   // input sex

   fields[6] = new_field(1, 8, 14, 2, 0, 0);  // label Previous
   fields[7] = NULL;

   // set field opts
   field_opts_off(fields[0], O_ACTIVE);
   set_field_buffer(fields[0], 0, "Input bodyweight:");

   field_opts_off(fields[1], O_AUTOSKIP);

   field_opts_off(fields[2], O_ACTIVE);
   set_field_buffer(fields[2], 0, "Input age (Optional):");

   field_opts_off(fields[3], O_AUTOSKIP);

   field_opts_off(fields[4], O_ACTIVE);
   set_field_buffer(fields[4], 0, "Input sex (M/f):");

   field_opts_off(fields[5], O_AUTOSKIP);

   field_opts_off(fields[6], O_EDIT);
   set_field_buffer(fields[6], 0, "Previous");

   FORM* form = new_form(fields);
   form_opts_off(form, O_BS_OVERLOAD);

   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < 3) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_LEFT:
            form_driver(form, REQ_PREV_CHAR);
            break;
         case KEY_RIGHT:
            form_driver(form, REQ_NEXT_CHAR);
            break;
         case 'q':
            unpost_form(form);
            free_form(form);
            for (int i = 0; i < 7; i++) free_field(fields[i]);
            clear();
            curs_set(false);
            return;
            break;
         case ENTER:
            if (selected == 3) {
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < 7; i++) free_field(fields[i]);
               clear();
               curs_set(false);
               return;
            } else {
               // updates the buffer
               form_driver(form, REQ_END_LINE);

               char* input_weight = field_buffer(fields[1], 0);
               char* input_age = field_buffer(fields[3], 0);
               char* input_sex = field_buffer(fields[5], 0);

               double bw = atof(input_weight);
               unsigned short age =
                   (!strcmp(input_age, "     ")) ? 23 : atoi(input_age);
               gender gend = (tolower(input_sex[0]) == 'f') ? female : male;

               unpost_form(form);
               clear();

               print_menu_5_score_show_results(input_lifts, estimated_1RMs,
                                               nb_lifts, gend, bw, age);

               post_form(form);
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:
            form_driver(form, ch);
            break;
      }

      box(stdscr, 2, 0);

      for (int i = 0; i < 7; i++) set_field_back(fields[i], A_NORMAL);

      (selected == 3) ? curs_set(false) : curs_set(true);

      set_field_back(fields[selected * 2], A_STANDOUT);

      refresh();
   } while ((ch = getch()));
}

void print_menu_5_score_show_results(lift lifts_selected[], double lifts_1RMs[],
                                     size_t nb_inputs, gender gend, double bw,
                                     unsigned short age) {
   int ch = 0, rows, cols;
   getmaxyx(stdscr, rows, cols);

   curs_set(false);

   // pre-calculates some values outside the loop
   double overall_score =
       getOverallScore(lifts_selected, lifts_1RMs, nb_inputs, gend, bw, age);

   double lift_scores[nb_inputs];
   for (int i = 0; i < (int)nb_inputs; i++) {
      lift_scores[i] =
          getScoreOfLift(lifts_selected[i], lifts_1RMs[i], gend, bw, age);
   }

   double symmetry_score = getSymmetryScore(lift_scores, nb_inputs);
   double est_total = getTotalFromScore(overall_score, gend, bw, age);
   double est_wilks = getWilksCoeff(gend, bw) * est_total;

   // here, we calculate the strongest and weakest lifts
   double cur_min = DBL_MAX, cur_max = DBL_MIN;
   lift lift_min = 0, lift_max = 0;
   for (int i = 0; i < (int)nb_inputs; i++) {
      if (cur_min > lift_scores[i]) {
         cur_min = lift_scores[i];
         lift_min = lifts_selected[i];
      }
      if (cur_max < lift_scores[i]) {
         cur_max = lift_scores[i];
         lift_max = lifts_selected[i];
      }
   }

   // calculate the expected lifts for all lifts assuming that the stregth is
   // the overall score
   double exp_lifts[nb_inputs];
   for (int i = 0; i < (int)nb_inputs; i++) {
      exp_lifts[i] =
          getLiftFromScore(overall_score, lifts_selected[i], gend, bw, age);
   }

   double* score_muscle_groups = getScoreForAllMuscleGroups(
       lifts_selected, lifts_1RMs, nb_inputs, gend, bw, age);

   do {
      switch (ch) {
         case KEY_BACKSPACE:
         case 'q':
            clear();
            free(score_muscle_groups);
            return;

         default:
            break;
      }

      box(stdscr, 2, 0);
      move(1, cols / 2 - 10);

      // shows overall score
      attron(A_BOLD);
      printw("OVERALL SCORE: %.1f", overall_score);
      attroff(A_BOLD);

      move(2, cols / 2 - 6);
      setColour(overall_score);
      printw("%s", getCategory(overall_score));
      attrset(COLOR_PAIR(0));

      // shows scores for all movement patterns
      move(2, 3);
      attron(A_BOLD);
      printw("SCORE OF LIFT TYPES:");
      attroff(A_BOLD);
      for (int i = 0; i < NB_LIFTTYPES; i++) {
         move(4 + i, 3);
         double liftType_score = getScoreForLiftType(
             (liftType)i, lifts_selected, lifts_1RMs, nb_inputs, gend, bw, age);
         if (liftType_score != -1) {
            printw("%s: %.1f [", liftType_names[i], liftType_score);
            setColour(liftType_score);
            printw("%s", getCategory(liftType_score));
            attrset(COLOR_PAIR(0));
            printw("]");
         } else {
            printw("%s: [No Inputs]", liftType_names[i]);
         }
      }

      // shows estimated 1RMs for each lift
      move(2, cols - 40);
      attron(A_BOLD);
      printw("ESTIMATED 1RMs:");
      attroff(A_BOLD);
      for (int i = 0; i < (int)nb_inputs; i++) {
         move(4 + i, cols - 40);
         printw("%s: %.1fkg [", lift_names[lifts_selected[i]], lifts_1RMs[i]);
         setColour(lift_scores[i]);
         printw("%s", getCategory(lift_scores[i]));
         attrset(COLOR_PAIR(0));
         printw("]");
      }

      // shows extra stats like the symmetry score,
      // estimated PL total, estimated wilks, strongest lift and weakest lift
      move(rows / 3 + nb_inputs, 3);
      attron(A_BOLD);
      printw("MORE STATS:");
      attroff(A_BOLD);

      move(rows / 3 + 2 + nb_inputs, 3);
      printw("Symmetry score: %.1f", symmetry_score);

      // Original only shows the total and wilks when the bench, deadlift and
      // squat are there, and uses those to calculate the numbers.
      // Here, it uses the overall score to calculate what the person should be
      // able to lift
      move(rows / 3 + 3 + nb_inputs, 3);
      printw("Estimated PL total: %.1f", est_total);

      move(rows / 3 + 4 + nb_inputs, 3);
      printw("Estimated Wilks: %.1f", est_wilks);

      move(rows / 3 + 5 + nb_inputs, 3);
      printw("Strongest Lift: %s", lift_names[lift_max]);

      move(rows / 3 + 6 + nb_inputs, 3);
      printw("Weakest Lift: %s", lift_names[lift_min]);

      // Print the relative strengths and weaknesses
      move(rows - nb_inputs * 2 - 4, 3);
      attron(A_BOLD);
      printw("STRENGTH VS AVERAGE:");
      attroff(A_BOLD);
      for (int i = 0; i < (int)nb_inputs; i++) {
         move(rows - nb_inputs * 2 - 2 + 2 * i, 3);
         printw("%s:", lift_names[lifts_selected[i]]);
         move(rows - nb_inputs * 2 - 2 + 2 * i + 1, 3);
         printw("You: %.1fkg Average: %.1fkg [", lifts_1RMs[i], exp_lifts[i]);

         // sets red or green
         (exp_lifts[i] > lifts_1RMs[i]) ? attrset(COLOR_PAIR(1) | A_BOLD)
                                        : attrset(COLOR_PAIR(2) | A_BOLD);

         printw("%.1f%%", -1 * (100 - 100 * (lifts_1RMs[i] / exp_lifts[i])));
         attrset(COLOR_PAIR(0));
         printw("]");
      }

      // for all muscle groups
      move(rows - NB_MUSCLE_GROUPS - 4, cols - 40);
      attron(A_BOLD);
      printw("MUSCLE SCORES:");
      attroff(A_BOLD);
      for (int i = 0; i < NB_MUSCLE_GROUPS; i++) {
         move(rows - NB_MUSCLE_GROUPS - 2 + i, cols - 40);
         printw("%s: %.1f [", muscle_group_names[i], score_muscle_groups[i]);
         setColour(score_muscle_groups[i]);
         printw("%s", getCategory(score_muscle_groups[i]));
         attrset(COLOR_PAIR(0));
         printw("]");
      }
   } while ((ch = getch()));
}

void setColour(double score) {
   unsigned short sum = 9;

   if (score >= 30) sum++;
   if (score >= 45) sum++;
   if (score >= 60) sum++;
   if (score >= 75) sum++;
   if (score >= 87.5) sum++;
   if (score >= 100) sum++;
   if (score >= 112.5) sum++;
   if (score >= 125) sum++;

   attrset(COLOR_PAIR(sum) | A_BOLD);
}

// Sets every to be normal after printing
void printColour(double score, const char* input) {
   setColour(score);
   printw("%s", input);
   attrset(COLOR_PAIR(0));
}
