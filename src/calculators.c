#include "header.h"

#define NB_ENTRIES_2_CALCULATORS 5
const char* entries_2_calculators[] = {"1RM", "Wilks", "TDEE",
                                       "Ideal Bodyweight", "Previous"};

void print_menu_3_1RM();
void print_menu_3_wilks();

void print_menu_2_calculators() {
   int selected = 0, ch = 0, rows, cols;

   do {
      clear();
      // processes the user input
      switch (ch) {
         case KEY_DOWN:
            if (selected < NB_ENTRIES_2_CALCULATORS - 1) {
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               selected--;
            }
            break;
         case 32:  // space bar
         case 10:  // enter bar
            switch (selected) {
               case 0:  // 1RM
                  clear();
                  print_menu_3_1RM();
                  break;
               case 1:  // Wilks
                  clear();
                  print_menu_3_wilks();

                  break;
               case 2:  // TDEE
                  break;
               case 3:  // Ideal bw
                  break;
               case 4:  // Previous
                  clear();
                  return;
                  break;
            }
            break;
         default:
            break;
      }

      // prints the menu
      box(stdscr, 2, 0);
      getmaxyx(stdscr, rows, cols);
      for (int i = 0; i < NB_ENTRIES_2_CALCULATORS; i++) {
         // centers the display
         move(rows / 2 - NB_ENTRIES_2_CALCULATORS / 2 + i, cols / 2 - 10);
         if (selected == i) {
            attron(A_STANDOUT);
            printw(entries_2_calculators[i]);
            attroff(A_STANDOUT);
         } else {
            printw(entries_2_calculators[i]);
         }
      }
      refresh();
   } while ((ch = getch()));
}

void print_menu_3_1RM() {
   int selected = 0, ch = 0, rows, cols;

   FIELD* fields[6];
   fields[0] = new_field(1, 14, 4, 18, 0, 0);   // label weight
   fields[1] = new_field(1, 10, 6, 18, 0, 0);   // input weight
   fields[2] = new_field(1, 17, 8, 18, 0, 0);   // label reps
   fields[3] = new_field(1, 10, 10, 18, 0, 0);  // input reps
   fields[4] = new_field(1, 8, 12, 18, 0, 0);   // label previous
   fields[5] = NULL;

   // various field options
   field_opts_off(fields[0], O_ACTIVE);
   set_field_buffer(fields[0], 0, "Weight lifted:");

   field_opts_off(fields[1], O_AUTOSKIP);

   field_opts_off(fields[2], O_ACTIVE);
   set_field_buffer(fields[2], 0, "Reps done (1-10):");

   field_opts_off(fields[3], O_AUTOSKIP);

   field_opts_off(fields[4], O_EDIT);
   set_field_buffer(fields[4], 0, "Previous");

   FORM* form = new_form(fields);
   // turns off going to previous lines
   form_opts_off(form, O_BS_OVERLOAD);
   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < 2) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_LEFT:
            form_driver(form, REQ_PREV_CHAR);
            break;
         case KEY_RIGHT:
            form_driver(form, REQ_NEXT_CHAR);
            break;
         case ENTER:
         case SPACE:
            if (selected == 2) {
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < 4; i++) free_field(fields[i]);
               clear();
               curs_set(false);
               return;
            } else {
               // reloads the form values
               form_driver(form, REQ_END_LINE);

               char* field_buf_0 = field_buffer(fields[1], 0);
               char* field_buf_1 = field_buffer(fields[3], 0);

               double input_weight = atof(field_buf_0);
               int input_reps = atoi(field_buf_1);

               if (input_weight <= 0) {
                  mvprintw(20, 100, "Invalid input weight");
               } else {
                  mvprintw(20, 100, "                    ");
               }
               if (input_reps <= 0 || input_reps > 10) {
                  mvprintw(21, 100, "Reps not in range 1-10");
               } else {
                  mvprintw(21, 100, "                      ");
               }

               mvprintw(22, 100, "Estimated one rep max: %.0f",
                        get1RM(input_weight, input_reps));
               form_driver(form, REQ_END_LINE);
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:

            if (selected < 3 && ch >= '0' && ch <= '9') {
               form_driver(form, ch);
            }
            break;
      }

      box(stdscr, 2, 0);

      for (int i = 0; i < 5; i++) set_field_back(fields[i], A_NORMAL);

      set_field_back(fields[selected * 2], A_STANDOUT);

      (selected == 2) ? curs_set(false) : curs_set(true);

      refresh();
   } while ((ch = getch()));
}

void print_menu_3_wilks() {
   int selected = 0, ch = 0, rows, cols;

   FIELD* fields[12];

   fields[0] = new_field(1, 16, 10, 18, 0, 0);  // label sex
   fields[1] = new_field(1, 4, 12, 18, 0, 0);   // input sex

   fields[2] = new_field(1, 17, 14, 18, 0, 0);  // label bw
   fields[3] = new_field(1, 4, 16, 18, 0, 0);   // input bw

   fields[4] = new_field(1, 12, 18, 18, 0, 0);  // label squat
   fields[5] = new_field(1, 4, 20, 18, 0, 0);   // input squat

   fields[6] = new_field(1, 18, 22, 18, 0, 0);  // label bench
   fields[7] = new_field(1, 4, 24, 18, 0, 0);   // input bench

   fields[8] = new_field(1, 15, 26, 18, 0, 0);  // label dl
   fields[9] = new_field(1, 4, 28, 18, 0, 0);   // input dl

   fields[10] = new_field(1, 8, 30, 18, 0, 0);  // label previous

   fields[11] = NULL;

   // specifying the field options
   field_opts_off(fields[0], O_ACTIVE);
   set_field_buffer(fields[0], 0, "Input Sex (M/F):");

   field_opts_off(fields[1], O_AUTOSKIP);

   field_opts_off(fields[2], O_ACTIVE);
   set_field_buffer(fields[2], 0, "Input bodyweight:");

   field_opts_off(fields[3], O_AUTOSKIP);

   field_opts_off(fields[4], O_ACTIVE);
   set_field_buffer(fields[4], 0, "Input squat:");

   field_opts_off(fields[5], O_AUTOSKIP);

   field_opts_off(fields[6], O_ACTIVE);
   set_field_buffer(fields[6], 0, "Input bench press:");

   field_opts_off(fields[7], O_AUTOSKIP);

   field_opts_off(fields[8], O_ACTIVE);
   set_field_buffer(fields[8], 0, "Input deadlift:");

   field_opts_off(fields[9], O_AUTOSKIP);

   field_opts_off(fields[10], O_EDIT);
   set_field_buffer(fields[10], 0, "Previous");

   FORM* form = new_form(fields);
   // turns off going to previous lines
   form_opts_off(form, O_BS_OVERLOAD);
   post_form(form);

   do {
      switch (ch) {
         case KEY_DOWN:
            if (selected < 5) {
               form_driver(form, REQ_NEXT_FIELD);
               form_driver(form, REQ_END_LINE);
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               form_driver(form, REQ_PREV_FIELD);
               form_driver(form, REQ_END_LINE);
               selected--;
            }
            break;
         case KEY_LEFT:
            form_driver(form, REQ_PREV_CHAR);
            break;
         case KEY_RIGHT:
            form_driver(form, REQ_NEXT_CHAR);
            break;
         case ENTER:
         case SPACE:
            if (selected == 5) {
               unpost_form(form);
               free_form(form);
               for (int i = 0; i < 11; i++) free_field(fields[i]);
               clear();
               curs_set(false);
               return;
            }
            break;
         case KEY_BACKSPACE:
            form_driver(form, REQ_DEL_PREV);
            break;
         default:
            if (selected == 0 || (ch >= '0' && ch <= '9')) {
               form_driver(form, ch);
            }
            break;
      }

      box(stdscr, 2, 0);

      for (int i = 0; i < 11; i++) set_field_back(fields[i], A_NORMAL);

      (selected == 5) ? curs_set(false) : curs_set(true);

      set_field_back(fields[2 * selected], A_STANDOUT);

      refresh();
   } while ((ch = getch()));
}
