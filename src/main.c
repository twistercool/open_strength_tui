#include "header.h"

#define NB_ENTRIES_1 5
const char* entries_1[] = {"Users", "Score Lifts", "Strength Standards",
                           "Calculators", "Exit"};

// menus organised in layers
void print_menu_1();
extern void print_menu_2_user();
extern void print_menu_2_score_select_lifts();
extern void print_menu_2_strength_standards();
extern void print_menu_2_calculators();

int main() {
   initscr();
   noecho();
   keypad(stdscr, true);
   curs_set(false);

   if (has_colors()) {
      start_color();
      use_default_colors();
      assume_default_colors(-1, -1);

      // red and green
      init_pair(1, COLOR_RED, COLOR_BLACK);
      init_pair(2, COLOR_GREEN, COLOR_BLACK);

      init_color(9, 196, 15, 162);   // #C40FA2
      init_pair(9, 9, COLOR_BLACK);  // Subpar

      init_color(10, 93, 46, 243);     // #5D2EF3
      init_pair(10, 10, COLOR_BLACK);  // Untrained

      init_color(11, 53, 152, 220);    // #3598DC
      init_pair(11, 11, COLOR_BLACK);  // Novice

      init_color(12, 39, 206, 131);    // #27CE83
      init_pair(12, 12, COLOR_BLACK);  // Intermediate

      init_color(13, 171, 204, 29);    // #ABCC1D
      init_pair(13, 13, COLOR_BLACK);  // Proficient

      init_color(14, 229, 194, 42);    // #E5C22A
      init_pair(14, 14, COLOR_BLACK);  // Advanced

      init_color(15, 233, 158, 59);    // #E99E3B
      init_pair(15, 15, COLOR_BLACK);  // Exceptional

      init_color(16, 255, 96, 51);     // #FF6033
      init_pair(16, 16, COLOR_BLACK);  // Elite

      init_color(17, 246, 56, 79);     // #F6384F
      init_pair(17, 17, COLOR_BLACK);  // World Class
   } else {
      fprintf(
          stderr,
          "This terminal lacks colour support, and this could lead to problems "
          "in the program\n");
   }

   print_menu_1();

   endwin();
}

void print_menu_1() {
   int selected = 0, ch = 0, rows, cols;

   do {
      clear();

      // processes the user input
      switch (ch) {
         case KEY_DOWN:
            if (selected < NB_ENTRIES_1 - 1) {
               selected++;
            }
            break;
         case KEY_UP:
            if (selected > 0) {
               selected--;
            }
            break;
         case 'q':
            endwin();
            exit(0);
            break;
         case ENTER:  // enter bar
            switch (selected) {
               case 0:  // Users
                  break;
               case 1:  // Score Lifts
                  clear();
                  print_menu_2_score_select_lifts();
                  break;
               case 2:  // Strength Standards
                  clear();
                  print_menu_2_strength_standards();
                  break;
               case 3:  // Calculators
                  clear();
                  print_menu_2_calculators();
                  break;
               case 4:  // Exit
                  endwin();
                  exit(0);
                  break;
            }
            break;
         default:
            break;
      }

      // prints the menu
      box(stdscr, 2, 0);
      getmaxyx(stdscr, rows, cols);
      for (int i = 0; i < NB_ENTRIES_1; i++) {
         // centers the display
         move(rows / 2 - NB_ENTRIES_1 / 2 + i, cols / 2 - 10);
         if (selected == i) {
            attron(A_STANDOUT);
            printw("%s", entries_1[i]);
            attroff(A_STANDOUT);
         } else {
            printw("%s", entries_1[i]);
         }
      }
      refresh();

   } while ((ch = getch()));
}

